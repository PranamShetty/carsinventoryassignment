function sortCarModels(inventory){
    let result = []
        inventory.sort((a, b) => {
          if (a.car_model < b.car_model) {
            return -1;
          }
          if (a.car_model > b.car_model) {
            return 1;
          }
          return 0;
        });
      
        for (let i = 0; i < inventory.length; i++) {
          result.push(inventory[i].car_model);
        }


        return result
      }
      

      module.exports = sortCarModels

