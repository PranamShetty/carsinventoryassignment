
function getCarById(inventory,carID){

    if(!inventory) return []
    else if(inventory.length == 0) return []
    else if(!Array.isArray((inventory))) return []


    if(typeof(carID)!== 'number') return []
    else if(carID == null) return [] 
    else if(carID === undefined) return []
    else if (carID === "") return [];


  for(let index = 0 ; index<inventory.length ; index++){
    if(inventory[index]['id']==carID){

        return inventory[index]

    }


  }

}

module.exports = getCarById